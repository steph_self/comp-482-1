import java.util.*;
import java.io.*;

class SumOfThree{
  public static void main(String args[]) throws IOException{
    
    Scanner scanner = new Scanner(new File("inputFile.txt"));
    int []nums = new int [100]; 
    int i = 0; 
    int indexOfZero = 0;
    int flag = 0;
      
    while(scanner.hasNextInt())
    {                              
        nums[i++] = scanner.nextInt();
        if (nums[i-1] == 0 && flag == 0) {
           indexOfZero = i-1;
           flag = 1;
        }
    }
      
    int lenOfNumbers = indexOfZero;
    int lenOfTotalArr = i;
        
    for (i = indexOfZero + 1 ;i < lenOfTotalArr ;i++ ) {
      if(sumOf3(nums,lenOfNumbers,nums[i]))
        System.out.println(nums[i] + " " + "YES");
      else 
        System.out.println(nums[i] + " " + "NO");   
    }  
      
  }
   static boolean sumOf3(int A[],int arr_size, int sum) 
    {  
        for (int i = 0; i < arr_size - 2; i++) { 
  
            // Find pair in subarray A[i+1..n-1] 
            // with sum equal to sum - A[i] 
            HashSet<Integer> s = new HashSet<Integer>(); 
            int curr_sum = sum - A[i]; 
            for (int j = i + 1; j < arr_size; j++) { 
                if (s.contains(curr_sum - A[j])) {  
                    return true; 
                } 
                s.add(A[j]); 
            } 
        } 
        return false; 
    } 
  
}